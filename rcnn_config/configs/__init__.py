from . import base as baseline
from . import double_rcnn
from . import rcnn_centernet
from . import two_head_rcnn
from .import three_head_rcnn
from . import rcnn_two_roi_head


configurations = {}

configurations['baseline'] = baseline
configurations['double_rcnn'] = double_rcnn
configurations['rcnn_centernet'] = rcnn_centernet
configurations['two_head_rcnn'] = two_head_rcnn
configurations['three_head_rcnn'] = three_head_rcnn
configurations['rcnn_two_roi_head'] = rcnn_two_roi_head
